# Twitter Clone

## Features

Twitter Clone brings the core functionalities of Twitter to a custom, real-time platform:

- **Tweets**: Users can post tweets, view tweets from the people they follow, and explore tweets from across the platform.
- **Likes, Comments, and Retweets**: Interaction with tweets through likes, comments, and retweets.
- **Profile Pages**: Customizable user profiles with support for profile pictures and cover photos.
- **Following and Followers Support**: Users can follow others and gain followers to build their social network.
- **Instant Messaging in Real Time**: Real-time direct messaging between users for private conversations.
- **Group Chat Support**: Users can create and participate in group chats for collaborative discussions.
- **Profile Pictures**: Users can upload and change their profile pictures.
- **Cover Photos**: Users can customize their profiles with cover photos.

## Getting Started

### Prerequisites

Ensure you have the following installed before starting:

- Node.js
- npm (Node Package Manager)
- MongoDB
